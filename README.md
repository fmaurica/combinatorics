What is Combinatorics
=====================

`Combinatorics` is an API that provides means of expressing many combinatorial
problems in a standard way.

Its goal is not to make the computer quickly find a solution, since the
algorithm underneath is just a naive enumeration.
Instead, its goal is to make the user quickly express the problem.

Thus, `Combinatorics` is targeted for search spaces that are small enough to
the point that thinking of a complex but efficient algorithm might take more
time than naively brute-forcing.


How does Combinatorics work
===========================

`Combinatorics` can enumerate permutations, combinations and arrangements
(with or without repetitions) of numbers. Then for solving a particular problem,
that is for finding an object-solution within the space of combinatorial
objects of the problem, the user only needs to do two things:
1. Create an enumerator `Enumerator<T> enumerator`, the following
   ones are provided: `Power<T>`, `Arrangement<T>`, `Combination<T>` and
   `Permutation<T>`. A `Function<int[], T> mapper`, which maps
   an integer sequence to (ideally) one and only one object of the search space,
   must be given for constructing `enumerator`.
2. Choose whether the problem at hand is a decision problem 
   or an optimization problem.<br>
   2.1. Decision problem. Create a `SatSolver<T> solver` from the
   previous `enumerator` and from a `Predicate<T> isSolution` which checks
   whether an object of the search space is a solution.
   Then simply call `Optional<T> exists()`.<br>
   2.2. Optimization problem. Create a `Minimizer<T>` from the
   previous `enumerator`, from a `Predicate<T> isSolution` and
   from a `Function<T, Integer> costEvaluator`.
   Then simply call `Optional<T> computeMinimum()`.
   
Examples are provided in the
`edu.fmaurica.combinatorics.tests.usecases` package.

Releases
========

https://gitlab.com/fmaurica/combinatorics/wikis/Releases
