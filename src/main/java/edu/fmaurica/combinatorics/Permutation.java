package edu.fmaurica.combinatorics;

import java.util.function.Function;

public class Permutation<T> extends Arrangement<T> {
  public Permutation(final int valueLength, final Function<int[], T> mapper) {
    super(valueLength, valueLength - 1, mapper);
  }
}