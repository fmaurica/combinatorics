package edu.fmaurica.combinatorics;

import java.util.Optional;
import java.util.function.Predicate;

public class SatSolver<T> {
  private final Enumerator<T> enumerator;
  private final Predicate<T> tester;

  public SatSolver(final Enumerator<T> enumerator, final Predicate<T> tester) {
    this.enumerator = enumerator;
    this.tester = tester;
  }

  public Optional<T> exists() {
    T curCombObj = enumerator.getCurrentObject();
    do
      if (tester.test(curCombObj))
        return Optional.of(curCombObj);
      else {
        enumerator.next();
        curCombObj = enumerator.getCurrentObject();
      }
    while (enumerator.hasNext());
    if (tester.test(curCombObj)) // isLastReached
      return Optional.of(curCombObj);
    else
      return Optional.empty();
  }
}