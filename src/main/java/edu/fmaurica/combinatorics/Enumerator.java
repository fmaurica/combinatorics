package edu.fmaurica.combinatorics;

import java.util.function.Function;

public abstract class Enumerator<T> {
  protected int[] currentSequence;
  protected Function<int[], T> mapper;

  public Enumerator(final Function<int[], T> mapper) {
    this.mapper = mapper;
  }

  public T getCurrentObject() {
    // Cloning needed because next() modifies currentSequence in place.
    return mapper.apply(currentSequence.clone());
  }

  protected boolean hasNext() {
    return !isLast();
  }

  protected abstract void next();

  protected abstract boolean isLast();
}