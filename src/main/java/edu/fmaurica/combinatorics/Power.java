package edu.fmaurica.combinatorics;

import java.util.function.Function;

/**
 * A power sequence of integers.
 *
 * @author fmaurica
 *
 */
public class Power<T> extends Enumerator<T> {
  protected int max;

  public Power(Function<int[], T> mapper, int max) {
    super(mapper);
    this.max = max;
  }

  public Power(final int sequenceLength, final int max,
      final Function<int[], T> mapper) {
    this(mapper, max);
    currentSequence = new int[sequenceLength];
    for (int i = 0; i < currentSequence.length; i++)
      currentSequence[i] = 0;
  }

  @Override
  protected boolean isLast() {
    for (final int element : currentSequence)
      if (element != max)
        return false;
    return true;
  }

  @Override
  protected void next() {
    for (int i = currentSequence.length - 1; i >= 0; i--)
      if (currentSequence[i] + 1 <= max) {
        currentSequence[i] = currentSequence[i] + 1;
        for (int j = i + 1; j < currentSequence.length; j++)
          currentSequence[j] = 0;
        break;
      }
  }
}