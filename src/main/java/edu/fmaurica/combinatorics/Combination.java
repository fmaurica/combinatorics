package edu.fmaurica.combinatorics;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import edu.fmaurica.utils.ArraysInt;

public class Combination<T> extends Arrangement<T> {
  public Combination(final int sequenceLength, final int max,
      final Function<int[], T> mapper) {
    super(mapper, max);
    currentSequence = new int[sequenceLength];
    for (int i = 0; i < currentSequence.length; i++)
      currentSequence[i] = currentSequence.length - 1 - i;
  }

  @Override
  public void next() {
    for (int i = currentSequence.length - 1; i >= 0; i--) {
      /* Getting elements before i */
      final Set<Integer> before = new HashSet<>();
      for (int j = 0; j < i; j++)
        before.add(currentSequence[j]);
      /* Setting element at i, if applicable */
      if (!before.contains(currentSequence[i] + 1)) {
        currentSequence[i]++;
        /* Setting elements after i */
        for (int j = currentSequence.length - 1; j > i; j--)
          currentSequence[j] = currentSequence.length - 1 - j;
        return;
      }
    }
  }

  @Override
  protected boolean isValid() {
    return super.isValid() && isSorted();
  }

  private boolean isSorted() {
    return ArraysInt.isSorted(currentSequence, false);
  }
}