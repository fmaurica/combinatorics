package edu.fmaurica.combinatorics;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public class Arrangement<T> extends Power<T> {
  public Arrangement(Function<int[], T> mapper, int max) {
    super(mapper, max);
  }

  public Arrangement(final int sequenceLength, final int max,
      final Function<int[], T> mapper) {
    super(mapper, max);
    currentSequence = new int[sequenceLength];
    for (int i = 0; i < currentSequence.length; i++)
      currentSequence[i] = i;
  }

  @Override
  protected boolean isLast() {
    for (int i = 0; i < currentSequence.length; i++)
      if (currentSequence[i] != max - i)
        return false;
    return true;
  }

  @Override
  protected void next() {
    for (int i = currentSequence.length - 1; i >= 0; i--) {
      /* Getting elements before i */
      final Set<Integer> before =
        // we don't need ordering, but we do need constant time operations
        new HashSet<>();
      for (int j = 0; j < i; j++)
        before.add(currentSequence[j]);
      /* Setting element at i */
      int atI = currentSequence[i] + 1;
      while (before.contains(atI))
        atI++;
      /* Setting elements after i, if applicable */
      if (atI <= max) {
        currentSequence[i] = atI;
        before.add(atI);
        for (int j = i + 1; j < currentSequence.length; j++)
          for (int k = 0; k <= max; k++)
            if (!before.contains(k)) {
              currentSequence[j] = k;
              before.add(k);
              break;
            }
        return;
      }
    }
  }

  protected boolean isValid() {
    return isTwoByTwoDistinct();
  }

  private boolean isTwoByTwoDistinct() {
    for (int i = 0; i < currentSequence.length; i++)
      for (int j = i + 1; j < currentSequence.length; j++)
        if (currentSequence[i] == currentSequence[j])
          return false;
    return true;
  }
}