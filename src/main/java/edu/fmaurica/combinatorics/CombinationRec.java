package edu.fmaurica.combinatorics;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class CombinationRec<T> {
  private final int sequenceLength;
  private final int max;
  private final Function<int[], T> mapper;

  Optional<T> optSolution = Optional.empty();

  public CombinationRec(
      final int max, final int sequenceLength,
      final Function<int[], T> mapper) {
    super();
    this.max = max;
    this.sequenceLength = sequenceLength + 1;
    this.mapper = mapper;
  }

  public Optional<T> exists(final Predicate<T> tester) {
    final int arr[] = new int[sequenceLength + 1];
    for (int i = 0; i <= sequenceLength; i++)
      arr[i] = i;
    recSatSolve(arr, sequenceLength, max, tester);
    return optSolution;
  }

  private void recSatSolve(
      final int arr[], final int n, final int r,
      final Predicate<T> tester) {
    final int data[] = new int[r];
    recSatSolve(arr, data, 0, n - 1, 0, r, tester);
  }

  private void recSatSolve(
      final int arr[], final int data[],
      final int start, final int end, final int index, final int r,
      final Predicate<T> tester) {
    if (index == r) {
      if (tester.test(mapper.apply(data)))
        optSolution = Optional.of(mapper.apply(data.clone()/*crucial clone*/));
    } else
      for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
        data[index] = arr[i];
        recSatSolve(arr, data, i + 1, end, index + 1, r, tester);
      }
  }
}