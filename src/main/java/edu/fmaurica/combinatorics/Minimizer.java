package edu.fmaurica.combinatorics;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class Minimizer<T> {
  protected final Enumerator<T> enumerator;
  protected final Predicate<T> tester;
  protected final Function<T, Integer> evaluator;
  protected Optional<T> minimum = Optional.empty();

  public Minimizer(final Enumerator<T> enumerator, final Predicate<T> tester,
      final Function<T, Integer> evaluator) {
    this.enumerator = enumerator;
    this.tester = tester;
    this.evaluator = evaluator;
  }

  public Optional<T> computeMinimum() {
    minimize();
    return minimum;
  }

  protected void minimize() {
    do {
      updateMinimum();
      enumerator.next();
    } while (enumerator.hasNext());
    updateMinimum(); // last
  }

  private void updateMinimum() {
    if (isNewMinimum(enumerator.getCurrentObject()))
      minimum = Optional.of(enumerator.getCurrentObject());
    return;
  }

  protected boolean isNewMinimum(final T candidate) {
    if (!tester.test(candidate))
      return false;
    if (minimum.isPresent())
      return evaluator.apply(candidate) < evaluator.apply(minimum.get());
    else
      return true;
  }

  public Optional<Integer> getMinimumCost() {
    if (!minimum.isPresent())
      return Optional.empty();
    else
      return Optional.of(evaluator.apply(minimum.get()));
  }
}