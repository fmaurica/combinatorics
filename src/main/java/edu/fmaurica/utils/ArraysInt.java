package edu.fmaurica.utils;

public class ArraysInt {
  public static boolean isSorted(final int t[], final boolean isIncreasing) {
    for (int i = 1; i < t.length; i++)
      if (isIncreasing && t[i - 1] > t[i])
        return false;
      else if (!isIncreasing && t[i - 1] < t[i])
        return false;
    return true;
  }

  public static String toString(final int t[]) {
    String res = "[" + t[0];
    for (int i = 1; i < t.length; i++)
      res = res + "," + t[i];
    res = res + "]";
    return res;
  }
}