package edu.fmaurica.combinatorics.competition;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Enumerator;
import edu.fmaurica.combinatorics.Permutation;
import edu.fmaurica.combinatorics.SatSolver;

public class Dna {

  public static void main(final String[] args) {
    final List<String> input = new ArrayList<>();
    final Scanner scanner = new Scanner(System.in);
    final int nbLines = Integer.parseInt(scanner.nextLine());
    input.add(nbLines + "");
    for (int i = 0; i < nbLines; i++)
      input.add(scanner.nextLine());
    System.out.println(Dna.solve(input));
    scanner.close();
  }

  public static String solve(final List<String> input) {
    String resStr = "";
    /* *************************** Inputs ********************************** */
    final Object[] fragmentsObj = input.subList(1, input.size() /* strict */).toArray();
    final String[] fragments = new String[fragmentsObj.length];
    for (int i = 0; i < fragments.length; i++)
      fragments[i] = (String) fragmentsObj[i];

    /* ************************ Combinatorics ****************************** */
    /* ********** https://gitlab.com/fmaurica/combinatorics **************** */
    final Function<int[], String[]> mapper = sequence -> {
      final String res[] = new String[sequence.length];
      for (int i = 0; i < sequence.length; i++)
        res[i] = fragments[sequence[i]];
      return res;
    };
    final Enumerator<String[]> enumerator = new Permutation<>(fragments.length, mapper);
    final Predicate<String[]> solutionTester = combObj -> {
      final Hashtable<Character, Character> correspondance = new Hashtable<>();
      correspondance.put('A', 'T');
      correspondance.put('T', 'A');
      correspondance.put('C', 'G');
      correspondance.put('G', 'C');
      String noSpace = "";
      for (final String element : combObj)
        noSpace += element;
      /* Is complementary? */
      final int middle = noSpace.length() / 2;
      for (int i = 0; i < middle; i++) {
        final char c = noSpace.charAt(i);
        if (noSpace.charAt(middle + i) != correspondance.get(c))
          return false;
      }
      /* Are the two strands of same length? */
      final int length = noSpace.length();
      int currentLength = 0;
      for (final String element : combObj) {
        if (currentLength == length / 2)
          break;
        else if (currentLength > length / 2)
          return false;
        currentLength += element.length();
      }
      return true;
    };
    final SatSolver<String[]> satSolver = new SatSolver<>(enumerator, solutionTester);
    final Optional<String[]> resOpt = satSolver.exists();
    if (resOpt.isPresent()) {
      final String[] resArray = resOpt.get();
      int length = 0;
      for (final String element : resArray)
        length += element.length();
      String res = "";
      int currentLength = 0;
      for (final String element : resArray) {
        if (currentLength == length / 2)
          res = res.trim() + "#" + element + " ";
        else
          res += element + " ";
        currentLength += element.length();
      }
      resStr += res.trim();
    } else
      resStr += "UNSAT";
    return resStr;
  }

  @Test
  public void doTests() throws IOException {
    final String dirName = "dna";
    final URL url = this.getClass().getResource(dirName);
    final String path = url.getPath();
    final File dir = new File(path);
    final int nbTests = dir.listFiles().length / 2; // (input+output)/2
    for (int i = 1; i <= nbTests; i++) {
      final String inputiStr = path + "/input" + i + ".txt";
      final String inputi = new String(Files.readAllBytes(Paths.get(inputiStr)));
      final String outputiStr = path + "/output" + i + ".txt";
      final String outputi = new String(Files.readAllBytes(Paths.get(outputiStr)));
      Assert.assertEquals(outputi, Dna.solve(inputStrToList(inputi)));
    }
  }

  protected List<String> inputStrToList(final String inputStr) {
    return Arrays.asList(inputStr.split("\n"));
  }
}