package edu.fmaurica.combinatorics.tests.basics;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Combination;
import edu.fmaurica.combinatorics.SatSolver;

public class TestCombination extends TestCombinatorics {
  @Test
  public void testSat() {
    resSat = "";
    final Combination<int[]> enumerator = new Combination<>(3, 4,
        sequence -> sequence);
    final SatSolver<int[]> solver = new SatSolver<>(enumerator,
        combObj -> testSatisfiesSat(combObj));
    final Optional<int[]> res = solver.exists();
    if (!res.isPresent())
      resSat = resSat + "UNSAT";
    Assert.assertEquals("2,1,0,\n" + "SAT", resSat);
  }

  @Test
  public void testUnsat() {
    resUnsat = "";
    final Combination<int[]> enumerator = new Combination<>(3, 4,
        sequence -> sequence);
    final SatSolver<int[]> solver = new SatSolver<>(enumerator,
        combObj -> testSatisfiesUnsat(combObj));
    final Optional<int[]> res = solver.exists();
    if (!res.isPresent())
      resUnsat = resUnsat + "UNSAT";
    Assert.assertEquals("2,1,0,\n" + "3,1,0,\n" + "3,2,0,\n" + "3,2,1,\n"
        + "4,1,0,\n" + "4,2,0,\n" + "4,2,1,\n" + "4,3,0,\n" + "4,3,1,\n"
        + "4,3,2,\n" + "UNSAT", resUnsat);
  }
}
