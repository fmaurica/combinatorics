package edu.fmaurica.combinatorics.tests.basics;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Power;
import edu.fmaurica.combinatorics.SatSolver;

public class TestPower extends TestCombinatorics {

  @Test
  public void testSat() {
    resSat = "";
    final Power<int[]> enumerator = new Power<>(3, 2, sequence -> sequence);
    final SatSolver<int[]> solver = new SatSolver<>(enumerator, combObj -> testSatisfiesSat(combObj));
    final Optional<int[]> res = solver.exists();
    if (!res.isPresent())
      resSat = resSat + "UNSAT";
    Assert.assertEquals(resSat,
        "0,0,0,\n" + "0,0,1,\n" + "0,0,2,\n" + "0,1,0,\n" + "0,1,1,\n" + "0,1,2,\n" + "0,2,0,\n" + "0,2,1,\n"
            + "0,2,2,\n" + "1,0,0,\n" + "1,0,1,\n" + "1,0,2,\n" + "1,1,0,\n" + "1,1,1,\n" + "1,1,2,\n" + "1,2,0,\n"
            + "1,2,1,\n" + "1,2,2,\n" + "2,0,0,\n" + "SAT");
  }

  @Test
  public void testUnsat() {
    resUnsat = "";
    final Power<int[]> enumerator = new Power<>(3, 4, sequence -> sequence);
    final SatSolver<int[]> solver = new SatSolver<>(enumerator, combObj -> testSatisfiesUnsat(combObj));
    final Optional<int[]> res = solver.exists();
    if (!res.isPresent())
      resUnsat = resUnsat + "UNSAT";
    Assert.assertEquals(resUnsat,
        "0,0,0,\n" + "0,0,1,\n" + "0,0,2,\n" + "0,0,3,\n" + "0,0,4,\n" + "0,1,0,\n" + "0,1,1,\n" + "0,1,2,\n"
            + "0,1,3,\n" + "0,1,4,\n" + "0,2,0,\n" + "0,2,1,\n" + "0,2,2,\n" + "0,2,3,\n" + "0,2,4,\n" + "0,3,0,\n"
            + "0,3,1,\n" + "0,3,2,\n" + "0,3,3,\n" + "0,3,4,\n" + "0,4,0,\n" + "0,4,1,\n" + "0,4,2,\n" + "0,4,3,\n"
            + "0,4,4,\n" + "1,0,0,\n" + "1,0,1,\n" + "1,0,2,\n" + "1,0,3,\n" + "1,0,4,\n" + "1,1,0,\n" + "1,1,1,\n"
            + "1,1,2,\n" + "1,1,3,\n" + "1,1,4,\n" + "1,2,0,\n" + "1,2,1,\n" + "1,2,2,\n" + "1,2,3,\n" + "1,2,4,\n"
            + "1,3,0,\n" + "1,3,1,\n" + "1,3,2,\n" + "1,3,3,\n" + "1,3,4,\n" + "1,4,0,\n" + "1,4,1,\n" + "1,4,2,\n"
            + "1,4,3,\n" + "1,4,4,\n" + "2,0,0,\n" + "2,0,1,\n" + "2,0,2,\n" + "2,0,3,\n" + "2,0,4,\n" + "2,1,0,\n"
            + "2,1,1,\n" + "2,1,2,\n" + "2,1,3,\n" + "2,1,4,\n" + "2,2,0,\n" + "2,2,1,\n" + "2,2,2,\n" + "2,2,3,\n"
            + "2,2,4,\n" + "2,3,0,\n" + "2,3,1,\n" + "2,3,2,\n" + "2,3,3,\n" + "2,3,4,\n" + "2,4,0,\n" + "2,4,1,\n"
            + "2,4,2,\n" + "2,4,3,\n" + "2,4,4,\n" + "3,0,0,\n" + "3,0,1,\n" + "3,0,2,\n" + "3,0,3,\n" + "3,0,4,\n"
            + "3,1,0,\n" + "3,1,1,\n" + "3,1,2,\n" + "3,1,3,\n" + "3,1,4,\n" + "3,2,0,\n" + "3,2,1,\n" + "3,2,2,\n"
            + "3,2,3,\n" + "3,2,4,\n" + "3,3,0,\n" + "3,3,1,\n" + "3,3,2,\n" + "3,3,3,\n" + "3,3,4,\n" + "3,4,0,\n"
            + "3,4,1,\n" + "3,4,2,\n" + "3,4,3,\n" + "3,4,4,\n" + "4,0,0,\n" + "4,0,1,\n" + "4,0,2,\n" + "4,0,3,\n"
            + "4,0,4,\n" + "4,1,0,\n" + "4,1,1,\n" + "4,1,2,\n" + "4,1,3,\n" + "4,1,4,\n" + "4,2,0,\n" + "4,2,1,\n"
            + "4,2,2,\n" + "4,2,3,\n" + "4,2,4,\n" + "4,3,0,\n" + "4,3,1,\n" + "4,3,2,\n" + "4,3,3,\n" + "4,3,4,\n"
            + "4,4,0,\n" + "4,4,1,\n" + "4,4,2,\n" + "4,4,3,\n" + "4,4,4,\n" + "UNSAT");
  }

}
