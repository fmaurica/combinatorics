package edu.fmaurica.combinatorics.tests.basics;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Arrangement;
import edu.fmaurica.combinatorics.SatSolver;

public class TestArrangement extends TestCombinatorics {
  @Test
  public void testSat() {
    resSat = "";
    final Arrangement<int[]> enumerator = new Arrangement<>(3, 4,
        sequence -> sequence);
    final SatSolver<int[]> solver = new SatSolver<>(enumerator,
        combObj -> testSatisfiesSat(combObj));
    final Optional<int[]> res = solver.exists();
    if (!res.isPresent())
      resSat = resSat + "UNSAT";
    Assert.assertEquals("0,1,2,\n" + "0,1,3,\n" + "0,1,4,\n" + "0,2,1,\n"
        + "0,2,3,\n" + "0,2,4,\n" + "0,3,1,\n" + "0,3,2,\n" + "0,3,4,\n"
        + "0,4,1,\n" + "0,4,2,\n" + "0,4,3,\n" + "1,0,2,\n" + "1,0,3,\n"
        + "1,0,4,\n" + "1,2,0,\n" + "1,2,3,\n" + "1,2,4,\n" + "1,3,0,\n"
        + "1,3,2,\n" + "1,3,4,\n" + "1,4,0,\n" + "1,4,2,\n" + "1,4,3,\n"
        + "2,0,1,\n" + "SAT", resSat);
  }

  @Test
  public void testUnsat() {
    resUnsat = "";
    final Arrangement<int[]> enumerator = new Arrangement<>(3, 4,
        sequence -> sequence);
    final SatSolver<int[]> solver = new SatSolver<>(enumerator,
        combObj -> testSatisfiesUnsat(combObj));
    final Optional<int[]> res = solver.exists();
    if (!res.isPresent())
      resUnsat = resUnsat + "UNSAT";
    Assert.assertEquals("0,1,2,\n" + "0,1,3,\n" + "0,1,4,\n" + "0,2,1,\n"
        + "0,2,3,\n" + "0,2,4,\n" + "0,3,1,\n" + "0,3,2,\n" + "0,3,4,\n"
        + "0,4,1,\n" + "0,4,2,\n" + "0,4,3,\n" + "1,0,2,\n" + "1,0,3,\n"
        + "1,0,4,\n" + "1,2,0,\n" + "1,2,3,\n" + "1,2,4,\n" + "1,3,0,\n"
        + "1,3,2,\n" + "1,3,4,\n" + "1,4,0,\n" + "1,4,2,\n" + "1,4,3,\n"
        + "2,0,1,\n" + "2,0,3,\n" + "2,0,4,\n" + "2,1,0,\n" + "2,1,3,\n"
        + "2,1,4,\n" + "2,3,0,\n" + "2,3,1,\n" + "2,3,4,\n" + "2,4,0,\n"
        + "2,4,1,\n" + "2,4,3,\n" + "3,0,1,\n" + "3,0,2,\n" + "3,0,4,\n"
        + "3,1,0,\n" + "3,1,2,\n" + "3,1,4,\n" + "3,2,0,\n" + "3,2,1,\n"
        + "3,2,4,\n" + "3,4,0,\n" + "3,4,1,\n" + "3,4,2,\n" + "4,0,1,\n"
        + "4,0,2,\n" + "4,0,3,\n" + "4,1,0,\n" + "4,1,2,\n" + "4,1,3,\n"
        + "4,2,0,\n" + "4,2,1,\n" + "4,2,3,\n" + "4,3,0,\n" + "4,3,1,\n"
        + "4,3,2,\n" + "UNSAT", resUnsat);
  }
}
