package edu.fmaurica.combinatorics.tests.basics;

public class TestCombinatorics {

  protected String resUnsat = "";
  protected String resSat = "";
  // One var for testing SAT, and another one for UNSAT
  // So that tests can be performed simultaneously

  protected boolean testSatisfiesSat(final int[] sequence) {
    for (final int element : sequence)
      resSat = resSat + element + ",";
    resSat = resSat + "\n";
    if (sequence[0] == 2) {
      resSat = resSat + "SAT";
      return true;
    }
    return false;
  }

  protected boolean testSatisfiesUnsat(final int[] sequence) {
    for (final int element : sequence)
      resUnsat = resUnsat + element + ",";
    resUnsat = resUnsat + "\n";
    return false;
  }

}
