package edu.fmaurica.combinatorics.tests.usecases;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Power;
import edu.fmaurica.combinatorics.SatSolver;
import edu.fmaurica.utils.ArraysInt;

public class TestPancakes {
  private static void flipAtIdx(final int[] t, final int idx) {
    final int fliped[] = new int[idx + 1];
    for (int i = 0; i < fliped.length; i++)
      fliped[i] = t[idx - i];
    for (int i = 0; i < fliped.length; i++)
      t[i] = fliped[i];
  }

  private static int[] flipPancakes(final int[] pancakes, final int[] sequence) {
    final int res[] = pancakes.clone();
    for (final int element : sequence)
      TestPancakes.flipAtIdx(res, element);
    return res;
  }

  private String doTest(final int pancakes[], final int flipMax) {
    String resTest = "";

    /*
     * Problem statement:
     * https://questionsacm.isograd.com/codecontest/pdf/pancakes.pdf
     */
    // We are told that at most 7 flips are needed to sort
    // the stack of 6 pancakes
    // => only 5^7 possibilities which is very small
    // => Use of Combinatorics is ideal
    int flipNb = 1;
    do {
      final Power<int[]> enumerator = new Power<>(flipNb, pancakes.length - 1, sequence -> sequence);
      final SatSolver<int[]> solver = new SatSolver<>(enumerator,
          combObj -> ArraysInt.isSorted(TestPancakes.flipPancakes(pancakes, combObj), true));
      final Optional<int[]> res = solver.exists();
      if (res.isPresent()) {
        resTest = "SAT\n" + flipNb + "\n" + ArraysInt.toString(res.get());
        break;
      } else
        flipNb = flipNb + 1;
    } while (flipNb <= flipMax);

    return resTest;
  }

  @Test
  public void test1() {
    final int pancakes[] = { 45, 40, 35, 20, 25, 30 };
    final int flipMax = 7;
    final String resTest = doTest(pancakes, flipMax);
    Assert.assertEquals(resTest, "SAT\n2\n[5,2]");
  }

  @Test
  public void test2() {
    final int pancakes[] = { 25, 35, 20, 40, 45, 30 };
    final int flipMax = 7;
    final String resTest = doTest(pancakes, flipMax);
    Assert.assertEquals(resTest, "SAT\n5\n[1,4,5,1,3]");
  }
}
