package edu.fmaurica.combinatorics.tests.usecases;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Power;
import edu.fmaurica.combinatorics.SatSolver;
import edu.fmaurica.utils.ArraysInt;

public class TestShortestPlusConstraints {
  private static boolean isReachable(final int i, final int j, final int[] sequence, final int[][] graph) {
    int traveler = i;
    final Set<Integer> traveled = new HashSet<>();
    traveled.add(traveler);
    for (final int element : sequence)
      if (graph[traveler][element] == 1 && !traveled.contains(element + 1) && !traveled.contains(element - 1)) {
        traveler = element;
        traveled.add(traveler);
      } else
        return false;
    if (traveler == j)
      return true;
    else
      return false;
  }

  private String doTest(final int[][] graph, final int i, final int j) {
    String resTest = "";

    /*
     * Let G be a directed graph whose vertices are numbered from $0$ to $n-1$.
     * Given two vertices $i$ and $j$, what is the shortest path from $i$ to $j$
     * such that its length is at most 10 and if we visit a vertex $m$ then we
     * cannot visit neither $m-1$ nor $m+1$ (if they exist)?
     */
    final int nbVertices = graph.length;
    final int maxPathLength = 10;
    int pathLength = 1;
    do {
      final Power<int[]> enumerator = new Power<>(pathLength, nbVertices - 1, sequence -> sequence);
      final SatSolver<int[]> solver = new SatSolver<>(enumerator, combObj -> {
        if (TestShortestPlusConstraints.isReachable(i, j, combObj, graph))
          return true;
        else
          return false;
      });
      final Optional<int[]> res = solver.exists();
      if (res.isPresent()) {
        resTest = "SAT\n" + i + "->" + ArraysInt.toString(res.get());
        break;
      } else
        pathLength++;
    } while (pathLength <= maxPathLength);

    return resTest;
  }

  @Test
  public void test1() {
    final int[][] graph = { { 0, 1, 0, 0, 0, 1, 0 }, { 0, 0, 1, 1, 0, 0, 0 }, { 0, 0, 0, 1, 1, 1, 0 },
        { 1, 1, 0, 0, 0, 1, 0 }, { 1, 0, 1, 0, 1, 1, 0 }, { 1, 0, 1, 0, 1, 1, 0 }, { 0, 1, 1, 0, 0, 0, 0 } };
    final int i = 6, j = 0;
    final String resTest = doTest(graph, i, j);
    Assert.assertEquals(resTest, "SAT\n6->[2,4,0]");
  }

}
