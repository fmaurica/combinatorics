package edu.fmaurica.combinatorics.tests.usecases;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Minimizer;
import edu.fmaurica.combinatorics.Power;
import edu.fmaurica.utils.ArraysInt;

public class TestTyping {

  protected int cost(final String s, final int[] candidate) {
    final int charCost = 10;
    final int shiftCost = 7;
    final int capsCost = 13;
    boolean isCaps = false;
    int res = 0;
    for (int i = 0; i < candidate.length; i++) {
      final int curCaps = candidate[i];
      final int curChar = s.charAt(i);
      if (curCaps == 1) {
        isCaps = !isCaps;
        res += capsCost;
      }
      if (isCaps && Character.isLowerCase(curChar))
        res += shiftCost;
      else if (!isCaps && Character.isUpperCase(curChar))
        res += shiftCost;
      res += charCost;
    }
    return res;
  }

  private String doTest(final String s) {
    String resTest = "";

    final int sLength = s.length();
    final Power<int[]> enumerator = new Power<>(sLength, 1, sequence -> sequence);
    final Minimizer<int[]> minimizer = new Minimizer<>(enumerator, combObj -> true, combObj -> cost(s, combObj));
    final Optional<int[]> resOpt = minimizer.computeMinimum();
    if (resOpt.isPresent()) {
      final int[] res = resOpt.get();
      resTest = "SAT\n" + minimizer.getMinimumCost() + "\n" + ArraysInt.toString(res);
    } else
      System.out.println("UNSAT");

    return resTest;
  }

  @Test
  public void test1() {
    final String resTest = doTest("ceciEstUNTEsT");
    Assert.assertEquals(resTest, "SAT\nOptional[157]\n[0,0,0,0,0,0,0,1,0,0,0,0,0]");
  }

}