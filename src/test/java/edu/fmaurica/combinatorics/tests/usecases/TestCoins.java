package edu.fmaurica.combinatorics.tests.usecases;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import org.junit.Assert;
import org.junit.Test;

import edu.fmaurica.combinatorics.Minimizer;
import edu.fmaurica.combinatorics.Power;
import edu.fmaurica.utils.ArraysInt;

public class TestCoins {

  private String doTest(final int coins[], final int nbs[], final int target) {
    String resTest = "";

    /*
     * We have the following coins: { coin1: 0.10EUR, nb: 16; coin2: 0.20EUR, nb: 5;
     * coin3: 0.50EUR, nb: 3 }. We want to form EXACTLY 2.70EUR. What is the
     * combination with the maximum number of coins?
     *
     * Eg: { combination1 = 2*0.50EUR + 5*0.20EUR + 7*0.10EUR ==> 2+5+7=14 coins;
     * combination2 = 1*0.50EUR + 5*0.20EUR + 12*0.10EUR ==> 1+5+12=18 coins } ==>
     * combination2 is better than combination1
     */

    final Power<int[]> enumerator = new Power<>(coins.length, Arrays.stream(nbs).max().getAsInt(),
        sequence -> sequence);
    final Predicate<int[]> tester = sequence -> {
      for (int i = 0; i < sequence.length; i++)
        if (sequence[i] > nbs[i])
          return false;
      int total = 0;
      for (int i = 0; i < sequence.length; i++)
        total += sequence[i] * coins[i];
      if (total == target)
        return true;
      return false;
    };
    final Function<int[], Integer> evaluator = sequence -> {
      int total = 0;
      for (final int element : sequence)
        total += element;
      return -total;
    };
    final Minimizer<int[]> minimizer = new Minimizer<>(enumerator, tester, evaluator);
    final Optional<int[]> res = minimizer.computeMinimum();
    if (res.isPresent()) {
      resTest += "SAT\n";
      resTest += -minimizer.getMinimumCost().get() + "\n";
      resTest += ArraysInt.toString(res.get());
    } else
      resTest = "UNSAT";
    return resTest;
  }

  @Test
  public void test1() {
    final int coins[] = { 10, 20, 50 };
    final int[] nbs = { 16, 5, 3 };
    final int target = 270;
    final String resTest = doTest(coins, nbs, target);
    Assert.assertEquals(resTest, "SAT\n20\n[16,3,1]");
  }

}
