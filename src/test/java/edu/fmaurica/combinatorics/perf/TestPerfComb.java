package edu.fmaurica.combinatorics.perf;

import java.util.function.Predicate;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.fmaurica.combinatorics.Combination;
import edu.fmaurica.combinatorics.CombinationRec;
import edu.fmaurica.combinatorics.Enumerator;
import edu.fmaurica.combinatorics.SatSolver;
import edu.fmaurica.utils.ArraysInt;

public class TestPerfComb {
  private static final int R = 7;
  private static final int N = 50; /*N-1 actually since we take from [0,100]*/

  @BeforeClass
  public static void showSysConfig() {
    Runtime runtime = Runtime.getRuntime();

    System.out.println("MACHINE");
    System.out.println("Name: " + System.getProperty("os.name"));
    System.out.println("Version: " + System.getProperty("os.version"));
    System.out.println("Arch:" + System.getProperty("os.arch"));
    System.out.println("Proc: " + runtime.availableProcessors());

    System.out.println("\nJAVA");
    System.out.println("Version: " + System.getProperty("java.version"));
    System.out
        .println("Runtime: " + System.getProperty("java.runtime.version"));
    System.out.printf("TotalMem: %dMB\n", runtime.totalMemory() / 1000000);
    System.out.printf("FreeMem: %dMB\n", runtime.freeMemory() / 1000000);
    System.out.printf("MaxMem: %dMB\n", runtime.maxMemory() / 1000000);

    System.out.println("\nCOMB");
    System.out.println("R=" + R);
    System.out.println("N=" + N);
  }

  @Test
  public void iterative() {
    System.out.println("\nITERATIVE");
    long start = System.currentTimeMillis();
    Enumerator<int[]> enumerator = new Combination<>(R, N, t -> t);
    Predicate<int[]> tester = t -> false;
    SatSolver<int[]> solver = new SatSolver<>(enumerator, tester);
    solver.exists();
    long end = System.currentTimeMillis();
    System.out.printf("ExecTime: %dmillis\n", end - start);
  }

  @Test
  public void recSolveCombination_unsat() {
    System.out.println("\nRECURSIVE");
    long start = System.currentTimeMillis();
    CombinationRec<int[]> combRec = new CombinationRec<>(R, N, t -> t);
    combRec.exists(t -> false);
    long end = System.currentTimeMillis();
    System.out.printf("ExecTime: %dmillis\n", end - start);
  }

  private String out = "";
  @Test
  public void recSolveCombination_sat() {
    System.out.println("\nRECURSIVE (test sat)");
    var start = System.currentTimeMillis();
    var combRec = new CombinationRec<>(3, 5, t -> t);
    var optSolution = combRec.exists(t -> {
        String s = ArraysInt.toString(t);
        out += s + "\n";
        return s.equals("[1,2,4]");
      });
    Assert.assertEquals(
      "[0,1,2]\n" +
      "[0,1,3]\n" +
      "[0,1,4]\n" +
      "[0,1,5]\n" +
      "[0,2,3]\n" +
      "[0,2,4]\n" +
      "[0,2,5]\n" +
      "[0,3,4]\n" +
      "[0,3,5]\n" +
      "[0,4,5]\n" +
      "[1,2,3]\n" +
      "[1,2,4]\n" +
      "[1,2,5]\n" +
      "[1,3,4]\n" +
      "[1,3,5]\n" +
      "[1,4,5]\n" +
      "[2,3,4]\n" +
      "[2,3,5]\n" +
      "[2,4,5]\n" +
      "[3,4,5]\n", out);
    Assert.assertEquals("[1,2,4]", ArraysInt.toString(optSolution.get()));
    var end = System.currentTimeMillis();
    System.out.printf("ExecTime: %dmillis\n", end - start);
  }
}